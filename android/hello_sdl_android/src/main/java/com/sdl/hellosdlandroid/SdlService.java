package com.sdl.hellosdlandroid;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.smartdevicelink.managers.CompletionListener;
import com.smartdevicelink.managers.SdlManager;
import com.smartdevicelink.managers.SdlManagerListener;
import com.smartdevicelink.managers.file.filetypes.SdlArtwork;
import com.smartdevicelink.managers.lifecycle.LifecycleConfigurationUpdate;
import com.smartdevicelink.managers.screen.SoftButtonObject;
import com.smartdevicelink.managers.screen.SoftButtonState;
import com.smartdevicelink.managers.screen.choiceset.ChoiceCell;
import com.smartdevicelink.managers.screen.choiceset.ChoiceSet;
import com.smartdevicelink.managers.screen.choiceset.ChoiceSetSelectionListener;
import com.smartdevicelink.managers.screen.menu.VoiceCommand;
import com.smartdevicelink.managers.screen.menu.VoiceCommandSelectionListener;
import com.smartdevicelink.protocol.enums.FunctionID;
import com.smartdevicelink.proxy.RPCNotification;
import com.smartdevicelink.proxy.TTSChunkFactory;
import com.smartdevicelink.proxy.rpc.Alert;
import com.smartdevicelink.proxy.rpc.CancelInteraction;
import com.smartdevicelink.proxy.rpc.OnButtonEvent;
import com.smartdevicelink.proxy.rpc.OnButtonPress;
import com.smartdevicelink.proxy.rpc.OnHMIStatus;
import com.smartdevicelink.proxy.rpc.SetDisplayLayout;
import com.smartdevicelink.proxy.rpc.enums.AppHMIType;
import com.smartdevicelink.proxy.rpc.enums.FileType;
import com.smartdevicelink.proxy.rpc.enums.HMILevel;
import com.smartdevicelink.proxy.rpc.enums.InteractionMode;
import com.smartdevicelink.proxy.rpc.enums.Language;
import com.smartdevicelink.proxy.rpc.enums.PredefinedLayout;
import com.smartdevicelink.proxy.rpc.enums.PredefinedWindows;
import com.smartdevicelink.proxy.rpc.enums.TriggerSource;
import com.smartdevicelink.proxy.rpc.listeners.OnRPCNotificationListener;
import com.smartdevicelink.transport.BaseTransportConfig;
import com.smartdevicelink.transport.MultiplexTransportConfig;
import com.smartdevicelink.transport.TCPTransportConfig;
import com.smartdevicelink.util.DebugTool;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSink;
import okio.Okio;

public class SdlService extends Service {

    private static final String TAG = "SDL Service";

    private static final String APP_NAME = "Connected Travel Demo";
    private static final String APP_ID = "8678309";
    private static final String LOADING = "Loading...";

    private static final String ICON_FILENAME 			= "hello_sdl_icon.png";

    private static final int FOREGROUND_SERVICE_ID = 111;

	// TCP/IP transport config
	// The default port is 12345
	// The IP is of the machine that is running SDL Core
	private static final int TCP_PORT = 12349;
	private static final String DEV_MACHINE_IP_ADDRESS = "m.sdl.tools";

    // variable to create and call functions of the SyncProxy
    private SdlManager sdlManager = null;

    private JSONArray ticketMasterEvents;
    private JSONArray parkings;

    private static final String TICKET_MASTER_API_KEY = "YOUR_KEY_HERE";
    private static final String PARKWHIZ_BEARER_TOKEN = "YOUR_TOKEN_HERE";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            enterForeground();
        }
    }

    // Helper method to let the service enter foreground mode
    @SuppressLint("NewApi")
    public void enterForeground() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(APP_ID, "SdlService", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
                Notification serviceNotification = new Notification.Builder(this, channel.getId())
                        .setContentTitle("Connected through SDL")
                        .setSmallIcon(R.drawable.ic_sdl)
                        .build();
                startForeground(FOREGROUND_SERVICE_ID, serviceNotification);
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startProxy();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            stopForeground(true);
        }

        if (sdlManager != null) {
            sdlManager.dispose();
        }

        super.onDestroy();
    }

    private void startProxy() {
        // This logic is to select the correct transport and security levels defined in the selected build flavor
        // Build flavors are selected by the "build variants" tab typically located in the bottom left of Android Studio
        // Typically in your app, you will only set one of these.
        if (sdlManager == null) {
            Log.i(TAG, "Starting SDL Proxy");
            // Enable DebugTool for debug build type
            if (BuildConfig.DEBUG) {
                DebugTool.enableDebugTool();
            }
            BaseTransportConfig transport = null;
            if (BuildConfig.TRANSPORT.equals("MULTI")) {
                int securityLevel;
                if (BuildConfig.SECURITY.equals("HIGH")) {
                    securityLevel = MultiplexTransportConfig.FLAG_MULTI_SECURITY_HIGH;
                } else if (BuildConfig.SECURITY.equals("MED")) {
                    securityLevel = MultiplexTransportConfig.FLAG_MULTI_SECURITY_MED;
                } else if (BuildConfig.SECURITY.equals("LOW")) {
                    securityLevel = MultiplexTransportConfig.FLAG_MULTI_SECURITY_LOW;
                } else {
                    securityLevel = MultiplexTransportConfig.FLAG_MULTI_SECURITY_OFF;
                }
                transport = new MultiplexTransportConfig(this, APP_ID, securityLevel);
            } else if (BuildConfig.TRANSPORT.equals("TCP")) {
                transport = new TCPTransportConfig(TCP_PORT, DEV_MACHINE_IP_ADDRESS, true);
            } else if (BuildConfig.TRANSPORT.equals("MULTI_HB")) {
                MultiplexTransportConfig mtc = new MultiplexTransportConfig(this, APP_ID, MultiplexTransportConfig.FLAG_MULTI_SECURITY_OFF);
                mtc.setRequiresHighBandwidth(true);
                transport = mtc;
            }

            // The app type to be used
            Vector<AppHMIType> appType = new Vector<>();
            appType.add(AppHMIType.DEFAULT);

            // The manager listener helps you know when certain events that pertain to the SDL Manager happen
            // Here we will listen for ON_HMI_STATUS and ON_COMMAND notifications
            SdlManagerListener listener = new SdlManagerListener() {
                @Override
                public void onStart() {
                    // HMI Status Listener
                    sdlManager.addOnRPCNotificationListener(FunctionID.ON_HMI_STATUS, new OnRPCNotificationListener() {
                        @Override
                        public void onNotified(RPCNotification notification) {
                            OnHMIStatus onHMIStatus = (OnHMIStatus) notification;
                            if (onHMIStatus.getWindowID() != null && onHMIStatus.getWindowID() != PredefinedWindows.DEFAULT_WINDOW.getValue()) {
                                return;
                            }
                            if (onHMIStatus.getHmiLevel() == HMILevel.HMI_FULL && onHMIStatus.getFirstRun()) {
                                initScreenConfiguration();
                                displayMainScreen();
                            }
                        }
                    });
                }

                @Override
                public void onDestroy() {
                    SdlService.this.stopSelf();
                }

                @Override
                public void onError(String info, Exception e) {
                }

                @Override
                public LifecycleConfigurationUpdate managerShouldUpdateLifecycle(Language language) {
                    return new LifecycleConfigurationUpdate(APP_NAME, null, TTSChunkFactory.createSimpleTTSChunks(APP_NAME), null);
                }
            };

            // Create App Icon, this is set in the SdlManager builder
            SdlArtwork appIcon = new SdlArtwork(ICON_FILENAME, FileType.GRAPHIC_PNG, R.drawable.ct_logo, true);

            // The manager builder sets options for your session
            SdlManager.Builder builder = new SdlManager.Builder(this, APP_ID, APP_NAME, listener);
            builder.setAppTypes(appType);
            builder.setTransportType(transport);
            builder.setAppIcon(appIcon);
            sdlManager = builder.build();
            sdlManager.start();
        }
    }

    private void registerMainVoiceCommands() {
        List<VoiceCommand> voiceCommands = Arrays.asList(
                new VoiceCommand(Arrays.asList("Show me events", "List events", "Events"), new VoiceCommandSelectionListener() {
                    @Override
                    public void onVoiceCommandSelected() {
                        showLoadingScreenAndBeginLoadingTicketmasterEvents();
                    }
                }),
                new VoiceCommand(Arrays.asList("Show me parkings", "Find nearby parking spots", "Book parkings"), new VoiceCommandSelectionListener() {
                    @Override
                    public void onVoiceCommandSelected() {
                        showLoadingScreenAndBeginLoadingParkings();
                    }
                })
        );

        sdlManager.getScreenManager().setVoiceCommands(voiceCommands);
    }

    private void registerParkingVoiceCommands() {
        List<VoiceCommand> voiceCommands = Arrays.asList(
                new VoiceCommand(Arrays.asList("Book", "Confirm Booking"), new VoiceCommandSelectionListener() {
                    @Override
                    public void onVoiceCommandSelected() {
                        showAlert("Parking booking complete");
                    }
                }),
                new VoiceCommand(Arrays.asList("Go Back", "Exit", "Cancel"), new VoiceCommandSelectionListener() {
                    @Override
                    public void onVoiceCommandSelected() {
                        displayMainScreen();
                    }
                })
        );

        sdlManager.getScreenManager().setVoiceCommands(voiceCommands);
    }

    private void initScreenConfiguration() {
        SetDisplayLayout setDisplayLayoutRequest = new SetDisplayLayout();
        setDisplayLayoutRequest.setDisplayLayout(PredefinedLayout.TEXT_AND_SOFTBUTTONS_WITH_GRAPHIC.toString());
        sdlManager.sendRPC(setDisplayLayoutRequest);
    }

    /**
     * Use the Screen Manager to set the initial screen text and set the image.
     * Because we are setting multiple items, we will call beginTransaction() first,
     * and finish with commit() when we are done.
     */
    private void displayMainScreen() {
        registerMainVoiceCommands();

        sdlManager.getScreenManager().beginTransaction();
        sdlManager.getScreenManager().setTextField1(APP_NAME);
        sdlManager.getScreenManager().setTextField2("");
        sdlManager.getScreenManager().setTextField3("");
        sdlManager.getScreenManager().setSoftButtonObjects(Arrays.asList(
                new SoftButtonObject(
                        "EventsButton",
                        new SoftButtonState("EventsButton", "Events", null),
                        new SoftButtonObject.OnEventListener() {
                            @Override
                            public void onPress(SoftButtonObject softButtonObject, OnButtonPress onButtonPress) {
                                showLoadingScreenAndBeginLoadingTicketmasterEvents();
                            }

                            @Override
                            public void onEvent(SoftButtonObject softButtonObject, OnButtonEvent onButtonEvent) {
                            }
                        }),
                new SoftButtonObject(
                        "ParkingButton",
                        new SoftButtonState("ParkingButton", "Parking", null),
                        new SoftButtonObject.OnEventListener() {
                            @Override
                            public void onPress(SoftButtonObject softButtonObject, OnButtonPress onButtonPress) {
                                showLoadingScreenAndBeginLoadingParkings();
                            }

                            @Override
                            public void onEvent(SoftButtonObject softButtonObject, OnButtonEvent onButtonEvent) {
                            }
                        })
        ));

		sdlManager.getScreenManager().setPrimaryGraphic(new SdlArtwork(ICON_FILENAME, FileType.GRAPHIC_PNG, R.drawable.ct_logo, true));
        sdlManager.getScreenManager().commit(new CompletionListener() {
            @Override
            public void onComplete(boolean success) {
                if (success) {
                    Log.i(TAG, "welcome show successful");
                } else {
                    Log.i(TAG, "welcome show fail");
                }
            }
        });
    }

    private void showLoadingScreenAndBeginLoadingTicketmasterEvents() {
        showLoadingAlert(LOADING);
        loadEvents();
    }

    private void showLoadingScreenAndBeginLoadingParkings() {
        showLoadingAlert(LOADING);
        loadParkings();
    }

    private void loadEvents() {
        OkHttpClient okHttpClient = new OkHttpClient();

        final Request request = new Request.Builder()
                .url("https://app.ticketmaster.com/discovery/v2/events.json?countryCode=US&apikey=" + TICKET_MASTER_API_KEY)
                .get()
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                dismissCurrentLoadingAlert();
                showAlert("Loading error - " + e.getMessage());
                Log.e(TAG, "Data loading error", e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (!response.isSuccessful()) {
                    dismissCurrentLoadingAlert();
                    showAlert("Loading error - " + response.message());
                } else {
                    dismissCurrentLoadingAlert();

                    try {
                        displayEventsToSdl(response);
                    } catch (Exception e) {
                        showAlert("Loading error - " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void loadParkings() {
        String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String startTime = today + "T10:00";
        String endTime = today + "T16:00";

        OkHttpClient okHttpClient = new OkHttpClient();

        final Request request = new Request.Builder()
                .url("https://api.parkwhiz.com/v4/quotes/?q=coordinates:34.103244,-118.3285829 distance:1&start_time=" + startTime + "&end_time=" + endTime)
                .header("Authorization", "Bearer " + PARKWHIZ_BEARER_TOKEN)
                .header("X-Pagination-Total", "10")
                .get()
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                displayMainScreen();
                showAlert("Loading error - " + e.getMessage());
                Log.e(TAG, "Data loading error", e);
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (!response.isSuccessful()) {
                    showAlert("Loading error - " + response.message());
                    displayMainScreen();
                } else {
                    try {
                        displayParkingsToSdl(response);
                    } catch (Exception e) {
                        showAlert("Loading error - " + e.getMessage());
                        e.printStackTrace();
                    }

                    displayMainScreen();
                }
            }
        });
    }

    private void displayEventsToSdl(@NotNull Response response) throws Exception {
        Log.d(TAG, "Display Events");
        JSONObject responseJson = new JSONObject(response.body().string());

        JSONArray events = responseJson.getJSONObject("_embedded")
                .getJSONArray("events");
        this.ticketMasterEvents = events;

        int numOfEventsToLoad = Math.min(10, events.length());

        List<ChoiceCell> choices = new ArrayList<>(numOfEventsToLoad);

        for (int i = 0; i < numOfEventsToLoad; i++) {
            JSONObject event = events.getJSONObject(i);

            String name = event.getString("name");
            choices.add(new ChoiceCell(name, Collections.singletonList(name), null));
        }

        final ChoiceSet choiceSet = new ChoiceSet(
                "Select event",
                choices,
                new ChoiceSetSelectionListener() {
                    @Override
                    public void onChoiceSelected(ChoiceCell choiceCell, TriggerSource triggerSource, int rowIndex) {
                        onEventSelected(rowIndex);
                    }

                    @Override
                    public void onError(String error) {
                        Log.e(TAG, "Choice set error - " + error);
                    }
                }
        );

        sdlManager.getScreenManager().presentChoiceSet(choiceSet, InteractionMode.BOTH);
        Log.d(TAG, "Choice set sent");
    }

    private void displayParkingsToSdl(@NotNull Response response) throws Exception {
        Log.d(TAG, "Display Parkings");

        JSONArray parkingsList = new JSONArray(response.body().string());
        this.parkings = parkingsList;

        int numOfParkingsToDisplay = Math.min(10, parkingsList.length());

        List<ChoiceCell> choices = new ArrayList<>(numOfParkingsToDisplay);

        for (int i = 0; i < numOfParkingsToDisplay; i++) {
            JSONObject parking = parkingsList.getJSONObject(i);

            String name = parking.getJSONObject("_embedded").getJSONObject("pw:location").getString("name");
            choices.add(new ChoiceCell(name, Collections.singletonList(name), null));
        }

        final ChoiceSet choiceSet = new ChoiceSet(
                "Select parking",
                choices,
                new ChoiceSetSelectionListener() {
                    @Override
                    public void onChoiceSelected(ChoiceCell choiceCell, TriggerSource triggerSource, int rowIndex) {
                        onParkingSelected(rowIndex);
                    }

                    @Override
                    public void onError(String error) {
                        Log.e(TAG, "Choice set error - " + error);
                    }
                }
        );

        sdlManager.getScreenManager().presentChoiceSet(choiceSet, InteractionMode.BOTH);
        Log.d(TAG, "Choice set sent");
    }

    private void onEventSelected(int index) {
        try {
            JSONObject event = ticketMasterEvents.getJSONObject(index);
            String name = event.getString("name");

            showAlert("You have selected " + name);
        } catch (Exception e) {
            showAlert("Navigation error - " + e.getMessage());
            Log.e(TAG, "Navigation error", e);
        }
    }

    private void onParkingSelected(int index) {
        try {
            JSONObject parking = parkings.getJSONObject(index);
            final JSONObject embedded = parking.getJSONObject("_embedded");
            final JSONObject location = embedded.getJSONObject("pw:location");
            final String name = location.getString("name");
            final String id = parking.getString("location_id");

            JSONArray photos = location.getJSONArray("photos");
            if (photos.length() > 0) {
                String photoUrl = photos.getJSONObject(0).getJSONObject("sizes").getJSONObject("gallery").getString("URL");

                OkHttpClient okHttpClient = new OkHttpClient();

                final Request request = new Request.Builder()
                        .url(photoUrl)
                        .get()
                        .build();

                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        e.printStackTrace();
                        showParking(id, name, null);
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        if (!response.isSuccessful()) {
                            Log.e(TAG, "Image loading error - " + response.message());
                            showParking(id, name, null);
                        } else {
                            File imageFile = new File(getCacheDir(), "image.jpg");
                            BufferedSink sink = Okio.buffer(Okio.sink(imageFile));
                            sink.writeAll(response.body().source());
                            sink.close();

                            showParking(id, name, imageFile);
                        }
                    }
                });
            } else {
                showParking(id, name, null);
            }

        } catch (Exception e) {
            showAlert("Navigation error - " + e.getMessage());
            Log.e(TAG, "Navigation error", e);
        }
    }

    private void showParking(String id, String name, @Nullable File photo) {
        registerParkingVoiceCommands();

        sdlManager.getScreenManager().beginTransaction();
        sdlManager.getScreenManager().setTextField1(APP_NAME);
        sdlManager.getScreenManager().setTextField2("Parking");
        sdlManager.getScreenManager().setTextField3(name);
        sdlManager.getScreenManager().setSoftButtonObjects(Arrays.asList(
                new SoftButtonObject(
                        "BookParking",
                        new SoftButtonState("BookParking", "Book", null),
                        new SoftButtonObject.OnEventListener() {
                            @Override
                            public void onPress(SoftButtonObject softButtonObject, OnButtonPress onButtonPress) {
                                showAlert("Parking booking complete");
                            }

                            @Override
                            public void onEvent(SoftButtonObject softButtonObject, OnButtonEvent onButtonEvent) {
                            }
                        }),
                new SoftButtonObject(
                        "GoBack",
                        new SoftButtonState("GoBack", "Go Back", null),
                        new SoftButtonObject.OnEventListener() {
                            @Override
                            public void onPress(SoftButtonObject softButtonObject, OnButtonPress onButtonPress) {
                                displayMainScreen();
                            }

                            @Override
                            public void onEvent(SoftButtonObject softButtonObject, OnButtonEvent onButtonEvent) {
                            }
                        })
        ));

        if (photo == null) {
            sdlManager.getScreenManager().setPrimaryGraphic(null);
        } else {
            sdlManager.getScreenManager().setPrimaryGraphic(new SdlArtwork(
                    id, FileType.GRAPHIC_JPEG, Uri.fromFile(photo), false
            ));
        }
        sdlManager.getScreenManager().commit(new CompletionListener() {
            @Override
            public void onComplete(boolean success) {
                if (success) {
                    Log.i(TAG, "welcome show successful");
                } else {
                    Log.i(TAG, "welcome show fail");
                }
            }
        });
    }

    private void showAlert(String text) {
        Alert alert = new Alert();
        alert.setAlertText1(text);
        alert.setDuration(5000);
        sdlManager.sendRPC(alert);
    }

    private void showLoadingAlert(String text) {
        Alert alert = new Alert();
        alert.setAlertText1(text);
        alert.setDuration(3000);
        alert.setSoftButtons(null);
        alert.setProgressIndicator(true);
        sdlManager.sendRPC(alert);
    }

    private void dismissCurrentLoadingAlert() {
        sdlManager.sendRPC(new CancelInteraction(FunctionID.ALERT.getId()));
    }
}

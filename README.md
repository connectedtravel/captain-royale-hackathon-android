## ConnectedTravel's SDL demo for ParkWhiz and Ticketmaster display

### Usage

1. Checkout this repo
2. If you are on Windows, run `./gradlew buildWindowSymLinks` inside `android` folder
3. Open `android` folder in Android Studio
4. Enter your Ticketmaster and Parkwhiz API keys in `/android/hello_sdl_android/src/main/java/com/sdl/hellosdlandroid/SdlService.java`
5. Connect your phone to SDL compatible car (or use [Manticore](https://smartdevicelink.com/resources/manticore) simulator)
6. Run the app
7. App should pop up in the SDL interface

(This sample is based on [Official Android SDL Sample](https://github.com/smartdevicelink/sdl_java_suite/tree/93778656113f0023ec7025281b77e28000a1f5bd))